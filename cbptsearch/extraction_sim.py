# -*- coding: utf-8 -*-

"""Planet transit extraction for TESS simulated light curves
"""

import yaml
import pandas as pd
from cbptsearch.util import template_generator
from cbptsearch.sp.transits import extract_candidates

# Extract

def extract_sim_candidates_stage(simnr, template_width, template_depth, threshold):
    # Read lc
    lc = pd.read_csv('/home/marcelo/Documentos/MasterIA/TFM/Notebooks/simulated/lc/cbpsim{}.lc'.format(int(simnr)), sep='      ', names=['time', 'flux'])
    
    # Generate transit pulse template
    template = template_generator(template_width, template_depth)
    
    # Extract candidates from lightcurve
    return extract_candidates(lc, simnr, template, min_size=len(template)/3,\
            threshold=threshold, time_col_name='time', flux_col_name='flux')
        
def extract_sim_candidates(sim_log, template_widths, template_depths, thresholds):
    candidates = []
    for index, sim_log_entry in sim_log.iterrows():
        for template_width in template_widths:
            for template_depth in template_depths:
                for threshold in thresholds:
                    print('[{}] Extracting candidates for simnr: {}, tmp_width: {}, tmp_depth: {}, thld: {} ...'\
                          .format(index, sim_log_entry['simnr'], template_width, template_depth, threshold))
                    candidates.extend(extract_sim_candidates_stage(sim_log_entry['simnr'], template_width, template_depth, threshold))
    return candidates

# Clean

def is_repeated(c1, c2, margin):
    return (abs(c1['t0'] - c2['t0']) <= margin)

def clean_repeated_candidates(candidates):
    filtered_candidates = {}
    
    for candidate in candidates:
        if candidate['t0'] not in filtered_candidates:
            filtered_candidates[candidate['t0']] = candidate
        elif candidate['duration'] > filtered_candidates[candidate['t0']]['duration']:
            filtered_candidates[candidate['t0']] = candidate
    return list(filtered_candidates.values())

# Tagging

def get_transit_info(sim_log_entry):
    return [{ 't0': sim_log_entry['trtimes{}'.format(i)], 'SN': sim_log_entry['trSN{}'.format(i)]} for i in range(15) if sim_log_entry['trtimes{}'.format(i)] != 0.0]

def get_transits_info(sim_log):
    transits_info = {}
    for i in range(len(sim_log)):
        sim_log_entry = sim_log.iloc[i]
        transits_info[sim_log_entry['simnr']] = get_transit_info(sim_log_entry)
    return transits_info

def is_transit(candidate_times, transits):
    for transit in transits:
        print('Checking t0: {} t[0]: {} - t[n]: {} ... '.format(transit['t0'],\
                        candidate_times[0], candidate_times[len(candidate_times)-1]), end='')
        if candidate_times[0] < transit['t0'] and candidate_times[len(candidate_times)-1] > transit['t0']:
            print('YES')
            return True
        else:
            print('NO')
            
    return False

def transit_tagging(candidates, transits_info):
    extracted_transits = []
    for candidate in candidates:
        print('Tagging system: {} t0: {} ...'.format(candidate['pl_hostname'], candidate['t0']))
        if is_transit(candidate['df']['time'], transits_info[candidate['pl_hostname']]):
            candidate['y'] = 1
            extracted_transits.append(candidate)
            print('--- YES ---')
        else:
            print('--- NO ---')
    return extracted_transits
# Save

def candidates_save_format(candidates):
    for candidate in candidates:
        candidate['lc'] = {
            'time': candidate['df']['time'].to_numpy().tolist(),
            'flux': candidate['df']['flux'].to_numpy().tolist()
        }
        
        candidate['pl_hostname'] = str(int(candidate['pl_hostname']))
        candidate.pop('template', None) # Do not write template
        candidate.pop('df', None) # Do not write df key
    return candidates

# Full Extraction

def full_extraction():
    sim_log = pd.read_csv('/home/marcelo/Documentos/MasterIA/TFM/Notebooks/simulated/simalltr.csv', sep=';')
    template_widths = [30, 60, 80, 100, 120, 250, 500, 750, 1000]
    template_depths = [1]
    thresholds = [0.01]
    
    candidates = extract_sim_candidates(sim_log, template_widths, template_depths, thresholds)
    print('--- {} candidates extracted ---'.format(len(candidates)))
    
    clean_candidates = clean_repeated_candidates(candidates)
    print('--- {} candidates after cleaning---'.format(len(clean_candidates)))
    
    transits_info = get_transits_info(sim_log)
    extracted_transits = transit_tagging(clean_candidates, transits_info)
    print('--- {} extracted transits ---'.format(len(extracted_transits)))

    with open('/home/marcelo/Documentos/MasterIA/TFM/Notebooks/dataset/tess_sim_untagged_dataset_{}.yml'.format('20200724'), 'w') as file:
        yaml.dump(candidates_save_format(extracted_transits), file)

if __name__ == "__main__":
    full_extraction()