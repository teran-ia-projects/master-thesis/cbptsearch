# -*- coding: utf-8 -*-

"""Circumbinary Planet ML based Search
"""

import sys
import getopt

import pandas as pd

from pathlib import Path
from time import localtime, strftime

from ml.training import Model
from ml.preprocessing import array_format_unlabeled

from extraction import lc_candidates_extraction
from util import sktime_format_unlabeled_dataset

CLASS_MAPPING = {
    '2C_NO_BIN': {0: 'FP', 1: 'CBP'},
    '2C': {0: 'FP', 1: 'TRANSIT'},
    '3C': {0: 'FP', 1: 'CBP', 2: 'EB'},
    '3C_CBP_EB2': {0: 'FP', 1: 'CBP_EB2', 2: 'EB1'},
    '4C': {0: 'FP', 1: 'CBP', 2: 'EB2', 3: 'EB1'}
}

def extract_canidates(system, template_widths, template_depth, threshold):    
    candidates = []
    
    for width in template_widths:
        print('Extracting candidates for system {} and template width {}, depth {}, threshold {} ...'\
                  .format(system, width, template_depth, threshold))
        candidates.extend(lc_candidates_extraction(system, '/home/marcelo/Documentos/MasterIA/TFM/Notebooks', 
                                     width, template_depth, threshold, save=False))
    return candidates

def ml_preprocessing(candidates, ts_length, mission):
    for candidate in candidates:
        candidate['lc'] = { 'time': candidate['df']['time'], 'flux': candidate['df']['flux']}
    return sktime_format_unlabeled_dataset(array_format_unlabeled(candidates, ts_length=ts_length))

def inference(candidates, model_path, ts_length, mission):
    cls_model = Model(None)
    cls_model.load(model_path)    
    X = ml_preprocessing(candidates, ts_length, mission)
    return cls_model.predict(X)

def format_results(candidates, y, labeling_type):
    results = pd.DataFrame({'candidate': [], 'class': [], 'type': [], 
                        't0': [], 'tN': [], 'time': [], 'flux':[], 'template_width': []})
    for i, candidate in enumerate(candidates):
        predicted = y[i]
        time = candidate['df']['time'].to_numpy().tolist()
        results = results.append({'candidate': str(i), 'type': CLASS_MAPPING[labeling_type][predicted], 
                              't0': time[0], 'tN': time[len(time)-1],
                              'class': str(predicted), 'time': time,
                              'template_width': str(len(candidate['template'])),
                              'flux': candidate['df']['flux'].to_numpy().tolist()},\
                              ignore_index=True)
    return results

def save_results(system, candidates, y, labeling_type, results_dir):
    run_dir = '{}/search-{}-run-{}'.format(results_dir, system.lower(), strftime("%Y%m%d%_H%M%S", localtime()))
    Path(results_dir).mkdir(parents=False, exist_ok=True)
    Path(run_dir).mkdir(parents=False, exist_ok=True)
    results = format_results(candidates, y, labeling_type)
    results.to_csv('{}/transit-search-{}-results.csv'.format(run_dir, system.lower()), index=False)

def search(template_depth, threshold, mission, system, template_widths, model_path, ts_length, labeling_type, results_dir):
    # 1. Signal Processing candidate extraction (matched filter)
    candidates = extract_canidates(system, template_widths, template_depth, threshold)

    # 2. ML model inference
    print('Classifying candidates...')
    y = inference(candidates, model_path, ts_length, mission)

    # Save results
    print('Saving results...')
    save_results(system, candidates, y, labeling_type, results_dir)

def main(argv):
    template_depth = 1
    threshold = 0.01
    mission = 'TESS'
    system = 'TOI 1338'
    template_widths = [10, 14, 20, 24, 30, 34]
    model_path = '../models/model-example.pkl'
    ts_length = 70
    labeling_type = '4C'
    results_dir = '../results/inference'
    
    # Get CLI args
    try:
        opts, args = getopt.getopt(argv,"d:t:m:s:w:l:t:r:",["t-depth=","thld=","mission=","system=","t-width=","model=","length=","type=","out-dir="])
    except getopt.GetoptError:
        print('search.py --t-depth <template-depth> --thld <threshold> --mission <mission> --system <system> --t-width <template-widths> --model <model-path> --length <ts-length> --type <labeling-type> --out-dir <results-dir>')
        sys.exit(2)
        
    for opt, arg in opts:
        if opt in ("-d", "--t-depth"):
            template_depth = int(arg)
        elif opt in ("-t", "--thld"):
            threshold = float(arg)
        elif opt in ("-m", "--mission"):
            mission = arg
        elif opt in ("-s", "--t-width"):
            template_widths = [ int(w) for w in arg.split(',')]
        elif opt in ("-w", "--system"):
            system = arg
        elif opt in ("-l", "--model"):
            model_path = arg
        elif opt in ("-t", "--length"):
            ts_length = int(arg)
        elif opt in ("-r", "--type"):
            labeling_type = arg
        elif opt in ("-r", "--out-dir"):
            results_dir = arg
            
    print('Template depth: {}'.format(template_depth))
    print('Threshold: {}'.format(threshold))
    print('Mission: {}'.format(mission))
    print('System: {}'.format(system))
    print('Template widths: {}'.format(template_widths))
    print('Model path: {}'.format(model_path))
    print('TS length: {}'.format(ts_length))
    print('Labeling type: {}'.format(labeling_type))
    print('Results dir: {}'.format(results_dir))

    search(template_depth, threshold, mission, system, template_widths, model_path, ts_length, labeling_type, results_dir)

if __name__ == "__main__":
    main(sys.argv[1:])