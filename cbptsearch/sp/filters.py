import numpy as np
import pandas as pd

"""Light curves filters module
"""

def matched_filter(lc_df, template, flux_col_name='FLUX', \
                         cand_col_name='CAND_FLUX', threshold=0.01):
    
    # Extract flux array
    lc_np = lc_df[flux_col_name].to_numpy()
    
    # Remove mean
    template = template - template.mean()
    lc_np = lc_np - lc_np.mean()
    
    # Apply matched filter
    detection = np.convolve(lc_np, np.flip(template))
    max_detection = detection.max()
    detection_df = pd.DataFrame((detection[int(len(template)/2)-1:int(len(template)/(-2))]),\
                        index=lc_df.index, columns=['DETECTION']) # TODO: check shift 

    # Extract candidates
    lc_df['DETECTION'] = detection_df['DETECTION'] 
    lc_df[cand_col_name] = lc_df[lc_df['DETECTION'] > max_detection*threshold][flux_col_name]
        
    return lc_df