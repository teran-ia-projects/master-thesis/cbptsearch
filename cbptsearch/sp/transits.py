# -*- coding: utf-8 -*-

"""Planet transits handling functions module
"""

import yaml
import pandas as pd
import matplotlib.pyplot as plt

from pathlib import Path

from cbptsearch import util
from .lightcurves import flatten_lc_with_mask
from .filters import matched_filter

def load_exoplanets_table(koi_path, pl_1_star_path, pl_2_star_path):
    
    # Load KOI and confirmed exoplanets tables
    kois = pd.read_csv(koi_path)[['kepid', 'kepoi_name', 'kepler_name', \
                      'koi_period', 'koi_time0bk', 'koi_time0', \
                      'koi_duration', 'koi_depth', 'koi_model_snr']]
    pls_1_star = pd.read_csv(pl_1_star_path)[['pl_hostname', 'pl_name']]
    pls_2_star = pd.read_csv(pl_2_star_path)[['pl_hostname', 'pl_name']]
    
    # Add star number columns
    pls_1_star['stars_num'] = 1
    pls_2_star['stars_num'] = 2
    
    # Concat planets
    planets = pd.concat([pls_1_star, pls_2_star], sort=False)
    
    # Join koi's transit data for confirmed planets
    planets = pd.merge(planets, kois, left_on=  ['pl_name'],
                   right_on= ['kepler_name'], 
                   how = 'left')
    
    return planets[planets['kepid'].notnull()], planets['pl_hostname'].unique()

def load_cbp_transits_table(path):
    # Load CBP own transits table
    cbps = pd.read_csv(path)
    return cbps, cbps['pl_hostname'].unique()

def extract_transit_lc(lc_flatten, t0, duration_days, time_col_name='time', \
                    flux_col_name='flux'):
    # Transits time conditions
    lower_cond = lc_flatten[time_col_name] >= (t0 - duration_days)
    upper_cond = lc_flatten[time_col_name] <= (t0 + duration_days)
        
    # Extract transit
    return lc_flatten[ lower_cond & upper_cond][[flux_col_name, time_col_name]]


def extract_transit_using_cbp_table(lc, pl_info):
    transit = None
    
    try:
        pl_name = pl_info['pl_name']
        pl_hostname = pl_info['pl_hostname']
        duration_days = pl_info['transit_duration'] / 24
        t0 = pl_info['transit_t0']
            
        print("Extracting planet {} transit t0={} ...".format(pl_name, t0), end=" ")      
        
        # Light curve flatten
        lc_flatten = lc.flatten(return_trend=False).to_pandas()
            
        transit = {
                    't0': t0, 
                    'duration': pl_info['transit_duration'],
                    'pl_name': pl_name,
                    'pl_hostname': pl_hostname,
                    'stars_num': 2,
                    'type': 'confirmed',
                    'y': 1,
                    'df': extract_transit_lc(lc_flatten, t0, duration_days)
            }
        print("OK!")
    except:
         print("ERROR")   
    
    return transit

def extract_cbp_transits_using_table(lc, pls_info):
    transits = {}
    for index, pl_info in pls_info.iterrows():
        transit = extract_transit_using_cbp_table( \
                     lc, pl_info)
        if transit != None:
            if not pl_info['pl_name'] in transits:
                transits[pl_info['pl_name']] = []
            transits[pl_info['pl_name']].append(transit)
    return transits

def extract_transit_using_table(lc, pl_info):
    transit = None
    
    try:
        pl_name = pl_info['pl_name']
        pl_hostname = pl_info['pl_hostname']
        duration_days = pl_info['koi_duration'] / 24
        t0 = pl_info['koi_time0bk']
            
        print("Extracting planet {} transit t0={} ...".format(pl_name, t0), end=" ")           
        lc_flatten = flatten_lc_with_mask(lc, \
                    pl_info['koi_period'], \
                    t0, duration_days)
            
        transit = {
                    't0': t0, 
                    'duration': pl_info['koi_duration'],
                    'period': pl_info['koi_period'],
                    'pl_name': pl_name,
                    'pl_hostname': pl_hostname,
                    'stars_num': pl_info['stars_num'],
                    'type': 'confirmed',
                    'y': 1,
                    'df': extract_transit_lc(lc_flatten, t0, duration_days)
            }
        print("OK!")
    except:
         print("ERROR")   
    
    return transit
    
def extract_transits_using_table(lc, pls_info):
    transits = {}
    for index, pl_info in pls_info.iterrows():
        transit = extract_transit_using_table( \
                     lc, pl_info)
        if transit != None:
            transits[pl_info['pl_name']] = transit
    return transits

def save_tranists(transits, time_col_name='time', flux_col_name='flux', \
                  figsize=(8, 8), add_system_dir=False):
    for k, system_transits in transits.items():
        for transit in system_transits:
            save_transit(transit, figsize=figsize, add_system_dir=add_system_dir)  
        
def save_candidate(candidate, time_col_name='time', flux_col_name='flux', \
                 figsize=(4, 4), root_dir=None):
    stars_num = candidate['stars_num']
    transit_type = candidate['type']
    tr_df = candidate['df'].copy()
    t0 = candidate['t0']
    template = candidate['template']
    threshold = candidate['threshold']
    system = candidate['pl_hostname']
    template_width = len(template)
    template_depth = "{:.2f}".format(template[0] - template[int(template_width/2)])
    
    # Set base path
    root_dir = '' if root_dir == None else (root_dir + '/')
    base_path = '{}transits/{}/{}-star/template-{}-{}-{}/{}'.format(root_dir, transit_type, 
                     stars_num, template_depth, template_width, threshold, system)
    
    Path("{}/fig".format(base_path)).mkdir(parents=True, exist_ok=True)
    
    # Save YAML file
    transit_to_write = candidate.copy()
    transit_to_write['lc'] = {
        'time': tr_df[time_col_name].to_numpy().tolist(),
        'flux': tr_df[flux_col_name].to_numpy().tolist()
    }
    
    transit_to_write.pop('df', None) # Do not write df key
    
    with open('{}/{}-{}-transit-candidate.yml'.format(\
            base_path, system, t0), 'w') as file:
        yaml.dump(transit_to_write, file)
    
    # Save plot
    if not tr_df.empty:
        fig = tr_df.plot(x=time_col_name, y=flux_col_name, figsize=figsize).get_figure()
        fig.savefig('{}/fig/{}-{}-transit-candidate.png'\
                .format(base_path, system, t0), bbox_inches='tight')
        plt.close(fig)
    else:
        print('Empty Transit Dataframe')
        
def save_transit(transit, time_col_name='time', flux_col_name='flux', \
                 figsize=(8, 8), add_system_dir=False):
    pl_name = transit['pl_name']
    stars_num = transit['stars_num']
    transit_type = transit['type']
    tr_df = transit['df'].copy()
    t0 = transit['t0']
    
    # Set base path
    base_path = 'transits/{}/{}-star'.format(transit_type, stars_num)
              
    if add_system_dir:
        base_path = "{}/{}".format(base_path,  transit['pl_hostname'])

    # Save YAML file
    transit_to_write = transit.copy()
    transit_to_write['lc'] = {
        'time': tr_df[time_col_name].to_numpy().tolist(),
        'flux': tr_df[flux_col_name].to_numpy().tolist()
    }
    
    transit_to_write.pop('df', None) # Do not write df key
    
    with open('{}/{}-{}-transit.yml'.format(\
            base_path, pl_name, t0), 'w') as file:
        yaml.dump(transit_to_write, file)
    
    # Save plot
    if not tr_df.empty:
        fig = tr_df.plot(x=time_col_name, y=flux_col_name, figsize=figsize).get_figure()
        fig.savefig('{}/fig/{}-{}-transit.png'\
                .format(base_path, pl_name, t0), bbox_inches='tight')
        plt.close(fig)
    else:
        print('Empty Transit Dataframe')

def extract_candidates(lc_df, pl_hostname, template, min_size=10,
                               cand_col_name='CAND_FLUX',\
                               flux_col_name='FLUX',\
                               time_col_name='TIME',
                               threshold=0.01):
    
    # Apply matched filter
    lc_df = matched_filter(lc_df, template, \
                         flux_col_name=flux_col_name, \
                         cand_col_name=cand_col_name, threshold=threshold)
    
    # Split candidates by NaN
    cand_lcs = util.split_by_nan(lc_df, cand_col_name, flux_col_name, time_col_name)
    
    # Prepare candidates
    candidates = []
    i = 1

    for cand in cand_lcs:
        
        # Min candidate size filter        
        if cand.shape[0] < min_size:
            #print("Skipping candidate {}".format(cand.shape))
            continue
 
        #print("Cand: {}".format(cand.shape))       
 
        t0 = cand.iloc[int(len(cand)/2)][time_col_name]

        transit = {
            't0': float(t0), 
            'duration': float((cand.iloc[len(cand)-1][time_col_name] - cand.iloc[0][time_col_name])*24),
            'pl_name': "candidate-{}".format(i),
            'pl_hostname': pl_hostname,
            'type': 'candidate',
            'y': None,
            'stars_num': 2,
            'df': cand,
            'template': template.tolist(),
            'threshold': threshold
        }     
        
        print("Candidate {} t0 {}".format(i, t0))
        candidates.append(transit)
        i = i + 1
    return candidates
    
def extract_candidates_labeled(lc_df, transits_t0, pl_hostname, template,\
                               cand_col_name='CAND_FLUX',\
                               flux_col_name='FLUX',\
                               time_col_name='TIME',\
                               threshold=0.01):
    # Apply matched filter

    lc_df = matched_filter(lc_df, template, \
                         flux_col_name=flux_col_name, \
                         cand_col_name=cand_col_name, threshold=threshold)

    # Split candidates by NaN
    cand_lcs = util.split_by_nan(lc_df, cand_col_name)
    
    # Prepare candidates
    candidates = []
    
    for i, cand in enumerate(cand_lcs):
        t0 = cand.iloc[int(len(cand)/2)][time_col_name]

        transit = {
            't0': t0, 
            'duration': (cand.iloc[0][time_col_name] - cand.iloc[0][time_col_name])*24,
            'pl_name': "candidate-{}".format(i),
            'pl_hostname': pl_hostname,
            'type': 'candidate',
            'y': 1 if util.contains_transit(transits_t0, cand, time_col_name) else 0,
            'stars_num': 2,
            'df': cand,
            'template': template,
            'threshold': threshold
        }     
        
        print("Candidate {} t0 {}".format(i, t0))
        candidates.append(transit)
    return { pl_hostname: candidates }

def plot_candidates_labeled(candidates, y, col_name='FLUX'):
    colors = ['Red', 'Green']
    for i, item in enumerate(candidates):
        pd.DataFrame(item, columns=['FLUX']).plot(color=colors[y[i]])
        
def plot_candidates_predictions(candidates, y, prediction, col_name='FLUX'):
    colors = [['Blue', 'Red'], ['Orange', 'Green']]
    for i, item in enumerate(candidates):
        pd.DataFrame(item, columns=['FLUX']).plot(color=colors[prediction[i]][y[i]])
    
    
    
    
