# -*- coding: utf-8 -*-

"""Light curves helper functions
"""

import pandas as pd
import numpy as np
import lightkurve as lk
import astropy.table as at

def load_full_raw_system_lightcurve(system, source='lk_search', \
                                    mission='Kepler', paths=None,\
                                    limit=17):
    # Load light curve quarters
    lcfc = None
    targets = 1
    
    if source == 'lk_search':
        try:
            print("Downloading system {} ...".format(system), end=" ")
            lcsr = lk.search_lightcurvefile(system, mission=mission, limit=limit)
            lcfc = lcsr.download_all()
        
            if lcfc == None:
                print("ERROR")
            else:
                targets = len(lcsr.unique_targets)
                print("OK") if targets == 1 \
                    else print("SKIPPING because multiple targets {}"\
                           .format(targets))
        except Exception:
            print("ERROR")
    elif source == 'fits':
        for path in paths:
            lcfc.append(lk.open(path))
    else:
        for path in paths:
            lc_df = pd.read_csv(path)
            lcfc.append(lk.LightCurve.form_timeseries(at.TimeSeries.from_pandas(lc_df)))
    
    # Stitch quarters
    return lcfc.PDCSAP_FLUX.stitch() if lcfc != None \
        and targets == 1 else None

def load_full_clean_system_lightcurve(system, source='lk_search', mission='Kepler', \
                               paths=None, outliers_sigma=20, outliers_sigma_upper=4, \
                               limit=17):
    # Get raw light curve
    lc_raw = load_full_raw_system_lightcurve(system, source=source, \
                                            mission=mission, paths=paths, \
                                            limit=limit)
    
    # Remove outliers
    return lc_raw.remove_outliers(sigma=outliers_sigma, \
        sigma_upper=outliers_sigma_upper) if lc_raw != None \
        else None

def load_full_flatten_system_lightcurve(system, source='lk_search', mission='Kepler', \
                               paths=None, outliers_sigma=20, outliers_sigma_upper=4, \
                               return_trend=False, limit=17):
    # Get clean light curve
    lc_clean = load_full_clean_system_lightcurve(system, source=source, mission=mission, \
                                     paths=paths, outliers_sigma=outliers_sigma, \
                                     outliers_sigma_upper=outliers_sigma_upper, \
                                     limit=limit)
    
    # Flatten light curve
    return lc_clean.flatten(return_trend=return_trend)

def flatten_lc_with_mask(lc_clean, period, t0, duration_days):
    # Define mask
    temp_fold = lc_clean.fold(period, t0=t0)
    fractional_duration = duration_days / period
    phase_mask = np.abs(temp_fold.phase) < (fractional_duration * 1.5)
    transit_mask = np.in1d(lc_clean.time, temp_fold.time_original[phase_mask])
        
    # Light Curve flatten
    return lc_clean.flatten(return_trend=False, mask=transit_mask).to_pandas()

def load_full_clean_system_lightcurves(systems, source='lk_search', mission='Kepler', \
                               paths=None, outliers_sigma=20, outliers_sigma_upper=4, \
                               limit=17):
    lcs = {}
    for system in systems:
        lc = load_full_clean_system_lightcurve(system, \
                source='lk_search', mission='Kepler', \
                paths=paths, outliers_sigma=outliers_sigma, \
                outliers_sigma_upper=outliers_sigma_upper, limit=limit)
        if lc != None:
            lcs[system] = lc
    return lcs
    
def save_lc_as_fits(lc, lc_name, lc_type='raw', overwrite=True):
    lc.to_fits(path='{}/{}-lightcurve.fits'.format(lc_type, lc_name), \
               overwrite=overwrite)

def load_fits_lc(lc_name, lc_type='clean', root_dir=None):
    root_dir = '' if root_dir == None else (root_dir + '/')
    return lk.open('{}{}/{}-lightcurve.fits'.format(root_dir, lc_type, lc_name)).FLUX
    