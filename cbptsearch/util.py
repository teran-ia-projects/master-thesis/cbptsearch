"""Utilities functions
"""

import numpy as np
import pandas as pd

def split_by_nan_old(df, col_name):
    events = np.split(df, np.where(np.isnan(df[col_name]))[0])
    # removing NaN entries
    events = [ev[~np.isnan(ev[col_name])] for ev in events if not isinstance(ev, np.ndarray)]
    # removing empty DataFrames
    return [ev for ev in events if not ev.empty]

def sktime_format_dataset(X, y):
    X_series = []
    for elem in X:
        X_series.append(pd.Series(elem))
    return pd.DataFrame({ 'dim_0': X_series}), np.array(y)

def sktime_format_unlabeled_dataset(X):
    X_series = []
    for elem in X:
        X_series.append(pd.Series(elem))
    return pd.DataFrame({ 'dim_0': X_series})

def sktime_dataframe_to_array(sktime_df):
    df_array = []
    for index, row in sktime_df.iterrows():
        df_array.append(row['dim_0'].to_numpy().tolist())
    return df_array

def split_by_nan(df, cand_col_name, flux_col_name, time_col_name='time'):     
    series = []  
    current = { time_col_name: [], flux_col_name: [], 'orig_index': [] }
    is_last_nan = False   
    
    for index, row in df.iterrows():
        if np.isnan(row[cand_col_name]):
            if len(current[flux_col_name]) > 0 and not is_last_nan:
                #print("Serie: {}".format(pd.DataFrame(current).shape))
                series.append(pd.DataFrame(current))
                current = { time_col_name: [], flux_col_name: [], 'orig_index': [] }
            is_last_nan = True
        else:
            is_last_nan = False
            current['orig_index'].append(index)
            current[time_col_name].append(row[time_col_name])
            current[flux_col_name].append(row[cand_col_name])
    
    return series

def symmetric_edge_pad(length, x):
    l_pad = int((length-len(x))/2)
    r_pad = int((length-len(x))/2) if (length-len(x))%2 == 0 else int((length-len(x))/2) + 1
    return np.pad(x, (l_pad, r_pad), 'edge')

def contains_transit(transits_t0, df, col_name):
    for t0 in transits_t0:
        if df[col_name].iloc[0] <= t0 and df[col_name].iloc[len(df.index)-1] >= t0 :
            return True
    return False

def template_generator(width, depth):
    template = []
    
    for i in range(width*2):
        template.append(1 if (i < int(width/2) or i >= (3*width/2)) else 1-depth)
        
    return np.array(template)