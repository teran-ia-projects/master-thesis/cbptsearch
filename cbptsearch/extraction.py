# -*- coding: utf-8 -*-

"""Planet transit extraction for Kepler light curves
"""

from cbptsearch.util import template_generator
from cbptsearch.sp.transits import extract_transits_using_table, save_tranists, extract_candidates, save_candidate
from cbptsearch.sp.lightcurves import load_full_clean_system_lightcurve, save_lc_as_fits, load_fits_lc

def transit_extraction(pl_table, systems, offset=0):
        
    # Extract transits for each system light curves
    for i, system in enumerate(systems):
    
        if i < offset:
            continue
    
        print("- {} -----------------------------------".format(i))
    
        # Load & Save clean light curves
    
        lc_clean = load_full_clean_system_lightcurve(system)
        print(lc_clean)
    
        if lc_clean == None:
            continue
    
        save_lc_as_fits(lc_clean, system, lc_type='clean')
    
        # Extract & Save system transits
    
        transits = extract_transits_using_table(lc_clean, pl_table[pl_table['pl_hostname'] == system])
        if len(transits) == 0:
            print("Empty transits for system {}".format(system))
    
        save_tranists(transits, figsize=(6, 6))
    
def lc_candidates_extraction(system, root_dir, template_width, template_depth,
                             threshold, time_col_name='time', flux_col_name='flux', save=True):
    
    # Load lightcurve
    lc_df = load_fits_lc(system, lc_type='flatten',  root_dir=root_dir).to_pandas()
    
    # Generate transit pulse template
    template = template_generator(template_width, template_depth)
    
    # Extract candidates from lightcurve
    candidates = extract_candidates(lc_df, system, template, min_size=len(template)/3, 
            threshold=threshold, time_col_name=time_col_name, flux_col_name=flux_col_name)
        
    # Save 
    if save:
        for candidate in candidates:
            save_candidate(candidate, time_col_name=time_col_name,
                       flux_col_name=flux_col_name, root_dir=root_dir)
    
    return candidates

def extract_kepler():    
    systems = ['Kepler-16', 'Kepler-34', 'Kepler-35', 'Kepler-38', 'Kepler-47', 'Kepler-413', 'Kepler-453', 'Kepler-1647', 'Kepler-1661']
    template_depth = 1
    threshold = 0.01
    
    for system in systems:
        for width in [10, 14, 20, 24, 30, 34]:
            print('Extracting candidates for system {} and template width {}, depth {}, threshold {} ...'\
                  .format(system, width, template_depth, threshold))
            lc_candidates_extraction(system, '/home/marcelo/Documentos/MasterIA/TFM/Notebooks', 
                                     width, template_depth, threshold)

    
if __name__ == "__main__":
    extract_kepler()