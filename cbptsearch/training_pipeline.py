# -*- coding: utf-8 -*-

"""Time Series Classification (TSC) training pipeline module
"""

import sys
import yaml
import collections
import getopt
import time
import datetime
import traceback

import numpy as np

import ml.preprocessing as prep
import ml.imbalance as imb

from pathlib import Path
from time import localtime, strftime
from sklearn.metrics import classification_report, roc_auc_score

from joblib import Parallel, delayed

from cbptsearch import util

from ml.training import Classifier
from ml.training import Pipeline

def preprocesing(base_path, dataset_info, prep_info):
    
    # Load dataset
    print('Loading dataset {}...'.format(dataset_info['name']))
    dataset = prep.load_dataset('{}/{}'.format(base_path, dataset_info['path']))
    ts_length = prep_info['ts_length']['value']

    # Format dataset
    X, y, y_orig = prep.array_format_dataset(dataset, ts_length)
     
    # Dataset test split
    split_info = prep_info['split']
    split_type = split_info['type']
    split_params = split_info['params']
    
    print('Spliting using {}...'.format(split_type), end=' ')
    start_time = time.time()
    
    folds = prep.get_train_test_split_by_type(X, y, y_orig, split_type, split_params)
        
    # Balance dataset for each fold
    b_folds = []
    balance_info = prep_info['balancing']
    resample_type = None if balance_info is None else balance_info['type']
    resample_params = None if balance_info is None else balance_info['params']
    for i, fold in enumerate(folds): 
        X_train = fold[0]
        y_train = fold[2]
        
        if not balance_info is None:
            print('Balancing dataset for fold {} using {}...'.format(i, resample_type))
            X_train_b, y_train_b = imb.resample(resample_type, X_train, y_train, resample_params)
            print("Before Resampling: {}".format(collections.Counter(y_train)))
            print("After Resampling: {}".format(collections.Counter(y_train_b)))      
        else:
            X_train_b = X_train
            y_train_b = y_train
        
        b_folds.append((X_train_b, fold[1], y_train_b, fold[3], fold[4]))

    print('OK ({} min)'.format((time.time() - start_time) / 60))
    return b_folds
     
     
def train(X_train, y_train, cls_info, model_path):
    print('Training...', end=' ')
    start_time = time.time()
    classifier = Classifier(cls_info['name'], cls_info['lib'])
    model = classifier.train(X_train, y_train, cls_info['params'], model_path)
    print('OK ({} min)'.format((time.time() - start_time) / 60))
    return model

def train_pipeline(X_train, y_train, pipeline_info, model_path):
    pipeline = Pipeline(pipeline_info)
    model = pipeline.train(X_train, y_train, model_path)
    return model

def test(model, X_test, y_test, y_orig_test, pipeline_info, dataset_info, prep_info):
    print('Testing...', end=' ')
    start_time = time.time()
    if pipeline_info[0]['lib'] == 'sktime':
        X_test, y_test = util.sktime_format_dataset(X_test, y_test)
    
    predictions = model.predict(X_test)
    y_prob = model.predict_proba(X_test)
    
    metrics = model.get_metrics(predictions, y_prob, y_test, dataset_info['classes'])
    print(classification_report(y_test, predictions))
    
    if len(dataset_info['classes']) == 2:
        print('AuC Score: {}'.format(roc_auc_score(y_test, predictions)))
    else:
        print('AuC Score OvO: {}'.format(roc_auc_score(y_test, y_prob, average='macro', multi_class='ovo')))
        print('AuC Score OvR: {}'.format(roc_auc_score(y_test, y_prob, average='macro', multi_class='ovr')))

    if type(predictions).__module__ == np.__name__:
        predictions = predictions.tolist()
    
    print('OK ({} min)'.format((time.time() - start_time) / 60))
    return { 'metrics': metrics.all(), 'model': pipeline_info, 'dataset': dataset_info, 'prep': prep_info }, \
        { 'predictions': predictions, 'pred_proba': y_prob, 'X_test': util.sktime_dataframe_to_array(X_test), 
          'y_test': y_test.tolist(), 'y_orig_test': y_orig_test.tolist(),  'model': pipeline_info, 
          'dataset': dataset_info, 'prep': prep_info }
            
def train_fold(fold, i, dataset_info, prep_info, training_info, base_path, results_dir, save_model):
    
    # 1. Get train test datasets
    X_train = fold[0]
    X_test = fold[1]
    y_train = fold[2]
    y_test = fold[3]
    y_orig_test = fold[4]
    
    # 2. Get stage info
    pipeline_info = training_info['pipeline']    
    file_suffix = '-{}-{}-fold{}'.format(training_info['name'], dataset_info['name'], i)
    model_path = '{}/{}/models/model{}.pkl'.format(base_path, \
                            results_dir, file_suffix) if save_model else None

    # 3. Train

    model = train(X_train, y_train, pipeline_info[0], model_path) if len(pipeline_info) == 1 \
        else train_pipeline(X_train, y_train, pipeline_info, model_path)
        
    # 4. Testing
    metric_results, pred_results = test(model, X_test, y_test, y_orig_test,\
                                        pipeline_info, dataset_info, prep_info)
    
    with open('{}/{}/metrics-result{}.yml'.format(base_path, \
            results_dir, file_suffix), 'w') as file:
        yaml.dump(metric_results, file)
    
    with open('{}/{}/result{}.yml'.format(base_path, \
            results_dir, file_suffix), 'w') as file:
        yaml.dump(pred_results, file)

def execRun(name, base_path, results_dir, run_info, save_model):

    # 1. Create results dirs
    Path(base_path + '/' + results_dir).mkdir(parents=False, exist_ok=True)
    Path(base_path + '/' + results_dir + '/models' ).mkdir(parents=False, exist_ok=True)
    
    # 2. Preprocesing
    dataset_info =run_info['dataset']
    prep_info = run_info['preprocesing']
    folds = preprocesing(base_path, dataset_info, prep_info)
    
    # 3. Training & Test
    for training_info in run_info['training']:
        print('Training pipeline {} ...'.format(name))
        for i, fold in enumerate(folds):
            print('Fold {}:'.format(i))
            train_fold(fold, i, dataset_info, prep_info, training_info, base_path, results_dir, save_model)

def run_parallel(i, run_info, name, exec_time, base_path, save_model):
        try:
            print('---- Executing run-{} ----'.format(i))
            run_start_time = time.time()
            results_dir =  'results/training/{}-{}-{}'.format(name, i, exec_time)
            execRun(name, base_path, results_dir, run_info, save_model)
        except Exception as e:
            print('ERROR in run-{}: {}'.format(i, e))
            traceback.print_exc()
        finally:
            print('---- Run-{} Finished ({} min) ----'.format(i, (time.time() - run_start_time) / 60))

def main(argv):
    print('-----> Strating execution at {} <-----'.format(datetime.datetime.now()))
    start_time = time.time()
    exec_time = strftime("%Y%m%d%_H%M%S", localtime())
    base_path = '../'
    run_file = 'run-example.yml'
    save_model = True
    n_jobs = 1

    # 1 Get CLI args
    try:
        opts, args = getopt.getopt(argv,"d:f:s:n:",["basedir=","file=","save=","njobs="])
    except getopt.GetoptError:
        print('training_pipeline.py -d <basedir> -f <runfilename> -s <saveflag> -n <njobs>')
        sys.exit(2)
        
    for opt, arg in opts:
        if opt in ("-d", "--basedir"):
            base_path = arg
        elif opt in ("-f", "--file"):
            run_file = arg
        elif opt in ("-s", "--save"):
            save_model = True if arg != None and arg.lower() == 'true' else False
        elif opt in ("-n", "--njobs"):
            j_jobs = int(arg)
            
    print('Base dir: {}'.format(base_path))
    print('Run file: {}'.format(run_file))

    # 2. Open run info file
    with open('{}/runs/{}'.format(base_path, run_file)) as file:
        runs_info = yaml.load(file)
    
    # 3. Execute runs
    Parallel(n_jobs=n_jobs)(delayed(run_parallel)(i, run_info, runs_info['name'], exec_time, base_path, save_model)\
            for i, run_info in enumerate(runs_info['runs']))

    print("--- Execution Time {} min ---".format((time.time() - start_time) / 60))
    print('-----> Finishing execution at {} <-----'.format(datetime.datetime.now()))

if __name__ == "__main__":
    main(sys.argv[1:])