# -*- coding: utf-8 -*-

"""Dataset Tagging functions
"""

import pandas as pd

def create_true_labeled_ds_entries(pl_names, flux_col_name='FLUX',\
                                   x_path_pattern='dataset/{}-transit.csv'):
    
    # 1. Load time series
    X = []
    for pl_name in pl_names:
        df = pd.read_csv(x_path_pattern.format(pl_name))
        X.append(df[flux_col_name].to_numpy())
    
    # 2. Load classes
    y = [1 for i in range(len(X))]
    
    return X, y


def create_labeled_ds_entries(y_path, candidates, y_col_name='y', flux_col_name='FLUX',\
                                   x_path_pattern='dataset/{}-transit.csv'):
    # 1. Load time series
    X = []
    for candidate in candidates:
        df = pd.read_csv(x_path_pattern.format(candidate))
        X.append(df[flux_col_name].to_numpy())
    
    # 2. Load classes
    
    y = []
    candidates_y = pd.read_csv(y_path)
    for y_candidate in candidates_y[y_col_name].to_numpy():
        y.append(1 if y_candidate == True else 0)
    
    return X, y