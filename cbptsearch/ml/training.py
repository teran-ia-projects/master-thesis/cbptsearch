# -*- coding: utf-8 -*-

"""TSC training algorithms module
"""

import pickle as pkl

import tslearn.neighbors as tslnb
from tslearn.shapelets import ShapeletModel

from sktime.classification.distance_based import KNeighborsTimeSeriesClassifier
from sktime.classification.distance_based import ElasticEnsemble
from sktime.classification.distance_based import ProximityForest
from sktime.classification.dictionary_based import BOSSEnsemble
from sktime.classification.shapelet_based import ShapeletTransformClassifier

from sktime.transformers.series_as_features.shapelets import ContractedShapeletTransform

import sklearn.pipeline as skl
from sklearn.ensemble import RandomForestClassifier

from cbptsearch.ml.metrics import MLMetrics

from cbptsearch import util

_classification_models = {
    'tslearn': {
        'knn': tslnb.KNeighborsTimeSeriesClassifier,
        'shapelets': ShapeletModel
    },
    'sktime': {
        'knn': KNeighborsTimeSeriesClassifier,
        'pf': ProximityForest,
        'shapelets': ContractedShapeletTransform,
        'stc': ShapeletTransformClassifier,
        'boss': BOSSEnsemble,
        'ee': ElasticEnsemble
    },
    'sklearn': {
        'rf': RandomForestClassifier
    }
}

class Model():
    
    def __init__(self, model):
        self._model = model
    
    def predict(self, to_predict):
        return  self._model.predict(to_predict)
    
    def predict_proba(self, to_predict):
        return  self._model.predict_proba(to_predict)
    
    def get_metrics(self, predictions, pred_proba, y, classes):
        return MLMetrics(predictions, pred_proba, y, classes)
    
    def save(self, path):
        pkl.dump(self._model, open(path, 'wb'))
        
    def load(self, path):
        with open(path,'rb') as file:
            self._model = pkl.load(file)
        return self._model
        

class Classifier():

    def __init__(self, method, lib_name):
        self._method = method
        self._lib_name = lib_name
        
    
    def train(self, X, y, params, model_path=None):
        print("Train params: {}".format(params))
        
        if self._lib_name == 'sktime':
            X, y = util.sktime_format_dataset(X, y)
        
        classifier = _classification_models[self._lib_name][self._method]()\
            if params == None \
            else _classification_models[self._lib_name][self._method](**params)
        model = Model(classifier.fit(X, y))
        
        if model_path != None:
            model.save(model_path)
        
        return model


class Pipeline():
    def __init__(self, classifiers_info):
        self._classifiers_info = classifiers_info

    def _build_pipeline(self):
        elems = []
        for classifier_info in self._classifiers_info:
            lib_name = classifier_info['lib'] 
            classifier_name = classifier_info['name'] 
            params = classifier_info['params']
            elems.append(('{}-{}'.format(lib_name, classifier_name), \
                         _classification_models[lib_name][classifier_name](**params)))
        return skl.Pipeline(elems)
        
    def train(self, X, y, model_path=None):
        pipeline = self._build_pipeline()
        
        if self._classifiers_info[0]['lib'] == 'sktime': 
            X, y = util.sktime_format_dataset(X, y)
                    
        print('Pipeline: {}'.format(pipeline))
        model = Model(pipeline.fit(X, y))
        
        if model_path != None:
            model.save(model_path)
        
        return model
    