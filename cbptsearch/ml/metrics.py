# -*- coding: utf-8 -*-

"""Machine Learning classification metrics module
"""

import math
import numpy as np

from sklearn.metrics import confusion_matrix, roc_auc_score

class MLMetrics():

    def __init__(self, prediction, pred_proba, y, classes):
        self._is_binary = (len(classes) == 2)
        self.cm = confusion_matrix(y, prediction)
        
        # Binary / Multiclass micro-average

        self.fp, self.fn, self.tp, self.tn = self._calc_binary_values() \
            if self._is_binary else self._calc_micro_values()
        self.accuracy = self._calc_accuracy(self.fp, self.fn, self.tp, self.tn)
        self.sensitivity = self._calc_sensitivity(self.fn, self.tp)
        self.specificity = self._calc_specificity(self.fp, self.tn)
        self.precision = self._calc_precision(self.fp, self.tp)
        self.g_mean = self._calc_g_mean(self.sensitivity, self.specificity)
        self.f1_score = self._calc_f1_score(self.precision, self.sensitivity)
        
        # Multiclass macro-average
        
        self.metrics_by_class = self._calc_confusion_matrix_values_by_class(classes)

        self.metrics_by_class = self._calc_sensitivity_by_class(self.metrics_by_class)
        self.metrics_by_class = self._calc_specificity_by_class(self.metrics_by_class)
        self.metrics_by_class = self._calc_precision_by_class(self.metrics_by_class)
        self.metrics_by_class = self._calc_f1_score_by_class(self.metrics_by_class)
        self.metrics_by_class = self._calc_g_mean_by_class(self.metrics_by_class)

        self.sensitivity_macro = self._calc_macro_average_metric(self.metrics_by_class, 'sensitivity')
        self.specificity_macro = self._calc_macro_average_metric(self.metrics_by_class, 'specificity')
        self.precision_macro = self._calc_macro_average_metric(self.metrics_by_class, 'precision')
        self.g_mean_macro = self._calc_macro_average_metric(self.metrics_by_class, 'g_mean')
        self.f1_score_macro = self._calc_macro_average_metric(self.metrics_by_class, 'f1_score')
        
        # AuC score
        
        if self._is_binary:
            self.auc_score = roc_auc_score(y, prediction)
        else:
            self.auc_score_ovo = roc_auc_score(y, pred_proba, average='macro', multi_class='ovo')
            self.auc_score_ovr = roc_auc_score(y, pred_proba, average='macro', multi_class='ovr')

        
    def _calc_binary_values(self):
        return float(self.cm[0][1]), float(self.cm[1][0]), float(self.cm[1][1]), float(self.cm[0][0])
        
    def _calc_micro_values(self):
        TP = np.diag(self.cm)
        FP = self.cm.sum(axis=0) - TP
        FN = self.cm.sum(axis=1) - TP
        TN = self.cm.sum() - (FP  + FN + TP)
        return float(FP.sum()), float(FN.sum()), float(TP.sum()), float(TN.sum())
        
    def _calc_confusion_matrix_values_by_class(self, classes):
        cmv_by_class = {}
        for klass in classes:
            TP = self.cm[klass][klass]
            FN = self.cm[klass,:].sum() - TP
            FP = self.cm[:,klass].sum() - TP
            TN = self.cm.sum() - TP - FN - FP
            cmv_by_class[klass] = { 'tp': float(TP), 'fn': float(FN), 'fp': float(FP), 'tn': float(TN) }

            
        return cmv_by_class
        
    def _calc_accuracy(self, fp, fn, tp, tn):
        return (tp + tn)/(tp + tn + fn + fp)
    
    def _calc_sensitivity(self, fn, tp):
        return (tp)/(tp + fn)
    
    def _calc_sensitivity_by_class(self, values_by_class):
        for k, v in values_by_class.items():
            values_by_class[k]['sensitivity'] = self._calc_sensitivity(v['fn'], v['tp'])
        return values_by_class

    def _calc_specificity(self, fp, tn):
        return (tn)/(fp + tn)
    
    def _calc_specificity_by_class(self, values_by_class):
        for k, v in values_by_class.items():
            values_by_class[k]['specificity'] = self._calc_specificity(v['fp'], v['tn'])
        return values_by_class
        
    def _calc_precision(self, fp, tp):
        return tp / (tp + fp)
    
    def _calc_precision_by_class(self, values_by_class):
        for k, v in values_by_class.items():
            values_by_class[k]['precision'] = self._calc_precision(v['fp'], v['tp'])
        return values_by_class
        
    def _calc_f1_score(self, precision, sensitivity):
        return (2 * precision * sensitivity) / (precision + sensitivity)
    
    def _calc_f1_score_by_class(self, values_by_class):
        for k, v in values_by_class.items():
            values_by_class[k]['f1_score'] = self._calc_f1_score(v['precision'], v['sensitivity'])
        return values_by_class
        
    def _calc_g_mean(self, sensitivity, specificity):
        return math.sqrt(sensitivity * specificity)
    
    def _calc_g_mean_by_class(self, values_by_class):
        for k, v in values_by_class.items():
            values_by_class[k]['g_mean'] = self._calc_g_mean(v['sensitivity'], v['specificity'])
        return values_by_class
    
    def _calc_macro_average_metric(self, values_by_class, metric_name):
        aggr_sum = 0
        for k, v in values_by_class.items():
            aggr_sum = aggr_sum + v[metric_name]
        return aggr_sum / len(values_by_class)
        
    def all(self):
        detail = { 'cm': self.cm.tolist(),
                   'fp': self.fp,
                   'fn': self.fn,
                   'tp': self.tp,
                   'tn': self.tn,
                   'by_class': self.metrics_by_class,
                   'accuracy': float(self.accuracy),
                   'sensitiviy': float(self.sensitivity),
                   'specificity': float(self.specificity),
                   'precision': float(self.precision),
                   'f_score': float(self.f1_score),
                   'g_mean': float(self.g_mean),
                   'auc_score': float(self.auc_score)
                 } \
            if self._is_binary else \
                 { 'cm': self.cm.tolist(),
                   'accuracy': float(self.accuracy),
                   'by_class': self.metrics_by_class,
                   'micro_avg': {
                       'sensitiviy': float(self.sensitivity),
                       'specificity': float(self.specificity),
                       'precision': float(self.precision),
                       'f_score': float(self.f1_score),
                       'g_mean': float(self.g_mean)
                   },
                   'macro_avg': {
                       'sensitiviy': float(self.sensitivity_macro),
                       'specificity': float(self.specificity_macro),
                       'precision': float(self.precision_macro),
                       'f_score': float(self.f1_score_macro),
                       'g_mean': float(self.g_mean_macro),
                       'auc_score_ovo': float(self.auc_score_ovo),
                       'auc_score_ovr': float(self.auc_score_ovr)
                   }
                 }
            
        return detail
    
    def _print_micro_macro_metrics(self, sensitivity, specificity, precision, f1_score, g_mean):
        print("Sensitivity: {}".format(sensitivity))
        print("Specificity: {}".format(specificity))
        print("Precision: {}".format(precision))
        print("F1-Score: {}".format(f1_score))
        print("G-Mean: {}".format(g_mean))
        
    def _print_binary_metrics(self):
        print("FP: {}, FN: {}, TP: {}, TN: {}".format(self.fp, self.fn, self.tp, self.tn))
        print("Accuracy: {}".format(self.accuracy))
        print("AuC-Score: {}".format(self.auc_score))
        self._print_micro_macro_metrics(self.sensitivity, self.specificity, \
                            self.precision, self.f1_score, self.g_mean)
        
    def _print_multiclass_metrics(self):
        print("Accuracy: {}".format(self.accuracy))
        print('------------ By Class --------------')
        for k, v in self.metrics_by_class.items():
            print('[Class {}]'.format(k))
            print("FP: {}, FN: {}, TP: {}, TN: {}".format(v['fp'], v['fn'], v['tp'], v['tn']))
            self._print_micro_macro_metrics(v['sensitivity'], v['specificity'], \
                            v['precision'], v['f1_score'], v['g_mean'])
        print('------------ Micro Avg -------------')
        self._print_micro_macro_metrics(self.sensitivity, self.specificity, \
                            self.precision, self.f1_score, self.g_mean)
        print('------------ Macro Avg -------------')
        self._print_micro_macro_metrics(self.sensitivity_macro, self.specificity_macro, \
                            self.precision_macro, self.f1_score_macro, self.g_mean_macro)
        print("AUC-Score OvR: {}".format(self.auc_score_ovr))
        print("AUC-Score OvO: {}".format(self.auc_score_ovo))
    def print_metrics(self):
        print('########## Result Metrics ##########')
        print("Confusion Matrix: {}".format(self.cm))
        if self._is_binary:
            self._print_binary_metrics()
        else:
            self._print_multiclass_metrics()
        print('####################################')
