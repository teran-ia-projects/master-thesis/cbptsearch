# -*- coding: utf-8 -*-

"""TSC Preprocessing functions module
"""

import yaml
import random

import numpy as np
import pandas as pd

import sklearn.model_selection as sklms

    
def train_test_split(X, y, y_orig, test_size=0.33, random_state=42):
    print('Test Size: {}, Random State: {}'.format(test_size, random_state))
    X_train, X_test, y_train, y_test, y_orig_train, y_orig_test = sklms.train_test_split(\
        X, y, y_orig, test_size=test_size, random_state=random_state)
    return [(np.asarray(X_train), np.asarray(X_test), np.asarray(y_train), \
             np.asarray(y_test), np.asarray(y_orig_test))]

def k_fold_split(X, y, y_orig, k, random_state):
    folds = []
    print('K: {}'.format(k))
    kf = sklms.KFold(n_splits=k)
    for train_i, test_i in kf.split(X, y):
        X_train = np.asarray([X[i] for i in train_i])
        y_train = np.asarray([y[i] for i in train_i])
        X_test = np.asarray([X[i] for i in test_i])
        y_test = np.asarray([y[i] for i in test_i])
        y_orig_test = np.asarray([y_orig[i] for i in test_i])
        folds.append((X_train, X_test, y_train, y_test, y_orig_test))
    return folds

def stratified_k_fold_split(X, y, y_orig, k, random_state):
    folds = []
    kf = sklms.StratifiedKFold(n_splits=k)
    for train_i, test_i in kf.split(X, y):
        X_train = np.asarray([X[i] for i in train_i])
        y_train = np.asarray([y[i] for i in train_i])
        X_test = np.asarray([X[i] for i in test_i])
        y_test = np.asarray([y[i] for i in test_i])
        y_orig_test = np.asarray([y_orig[i] for i in test_i])
        folds.append((X_train, X_test, y_train, y_test, y_orig_test))
    return folds

def dataset_train_test_split_by_system(dataset, test_systems):
    dataset_train = {}
    dataset_test = {}
        
    for k,v in dataset.items():
        if v['system'] in test_systems:
            dataset_test[k] = v
        else:
            dataset_train[k] = v
    return dataset_train, dataset_test

_train_test_split_types = {
    'k_fold': k_fold_split,
    'stratified_k_fold': stratified_k_fold_split,
    'train_test': train_test_split
}

def get_train_test_split_by_type(X, y, y_orig, split_type, split_params):
    return _train_test_split_types[split_type](X, y, y_orig, **split_params)

def balance_dataset_dict(dataset, classes, samples_by_class):
    # Init
    b_dataset = {}
    dataset_by_class = []
    
    for i in range(len(classes)):
        dataset_by_class.append([])
    
    # Dataset by class
    for k,v in dataset.items():
        for klass in classes:
            if v['y'] == klass:
                dataset_by_class[klass].append(v)

    # Dataset undersampling
    us_dataset_by_class = []     
    
    for i, class_dataset in enumerate(dataset_by_class):
        if i in samples_by_class:
            us_dataset_by_class.append(random.choices(class_dataset, k=samples_by_class[i]))
        else:
            us_dataset_by_class.append(class_dataset)
        
    for us_class_datset in us_dataset_by_class:
        for value in us_class_datset:
            b_dataset["{}_{}".format(value['system'], value['t0'])] = value

    return b_dataset

def balance_dataset(X, y, classes, samples_by_class):
    # Init 
    dataset_by_class = {}
    for kls in classes:
        dataset_by_class[kls] = []
    
    # Dataset by class
    for i, inst in enumerate(X):
        dataset_by_class[y[i]].append((inst, y[i]))

    # Dataset undersampling
    X_balanced = []
    y_balanced = []
    us_dataset_by_class = []
    
    for klass, class_dataset in dataset_by_class.items():
        if klass in samples_by_class:
            us_dataset_by_class.append(random.choices(class_dataset, k=samples_by_class[klass]))
        else:
            us_dataset_by_class.append(class_dataset)

    for us_class_dataset in us_dataset_by_class:
        for value in us_class_dataset:
            X_balanced.append(value[0])
            y_balanced.append(value[1])

    return X_balanced, y_balanced

def create_noise_array(length):
    array = []
    for i in range(length):
        array.append(1 + random.random()/10000)
    return array

def pad_with_noise(vector, pad_width, iaxis, kwargs):
    vector[:pad_width[0]] = create_noise_array(pad_width[0])
    vector[-pad_width[1]:] = create_noise_array(pad_width[1])

def symmetric_noise_pad(length, x):
    l_pad = int((length-len(x))/2)
    r_pad = int((length-len(x))/2) if (length-len(x))%2 == 0 else int((length-len(x))/2) + 1
    return np.pad(x, (l_pad, r_pad), pad_with_noise)

def symmetric_crop(length, x):
    extra = abs((length-len(x)))
    l_pad = int((extra)/2)
    r_pad = int(extra/2) if extra%2 == 0 else int(extra/2) + 1
    return x[l_pad:-r_pad]

def length_normalization(length, x):
    x_norm = x
    if len(x) < length:
        x_norm = symmetric_noise_pad(length, x)  
    elif len(x) > length:
        x_norm = symmetric_crop(length, x)
    return x_norm

def array_format_dataset(dataset, ts_length=50):
    X = []
    y = []
    y_orig = []
    dataset_array = dataset.values() if isinstance(dataset, dict) else dataset
    for v in dataset_array:
        X.append(length_normalization(ts_length, v['lc']['flux']))
        y.append(v['y'])
        y_orig.append(v['y_orig'])
    return X, y, y_orig

def array_format_unlabeled(dataset, ts_length=50):
    X = []
    dataset_array = dataset.values() if isinstance(dataset, dict) else dataset
    for v in dataset_array:
        X.append(length_normalization(ts_length, v['lc']['flux']))
    return X

def sktime_format_dataset_dict(dataset, ts_length=50):
    X_dim_0 = []
    y = []
    for k,v in dataset.items():
        X_dim_0.append(pd.Series(length_normalization(ts_length, v['lc']['flux'])))
        y.append(v['y'])
    return pd.DataFrame({ 'dim_0': X_dim_0 }), np.array(y)

def tess_candidate_resampling(candidate_lc):
    df = pd.DataFrame(candidate_lc)
    df.set_index('time', drop=False)['flux']
    df.index = pd.TimedeltaIndex(pd.to_timedelta(df.index, unit='h') / 6, freq="10T")
    df['flux'] = df.resample('30T')['flux'].mean()
    df['time'] = df.resample('30T')['time'].median()
    df = df.dropna()
    return { 'time': df['time'].to_numpy().tolist(), 'flux': df['flux'].to_numpy().tolist()}

def tess_lc_resampling(df):
    df.set_index('time', drop=False)['flux']
    df.index = pd.TimedeltaIndex(pd.to_timedelta(df.index, unit='h') / 6, freq="10T")
    df['flux'] = df.resample('30T')['flux'].mean()
    df['time'] = df.resample('30T')['time'].median()
    df = df.dropna()
    return df

def load_dataset(path):
    dataset = {}
    with open(path) as file:
        dataset = yaml.load(file, Loader=yaml.FullLoader)
    return dataset