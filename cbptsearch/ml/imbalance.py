# -*- coding: utf-8 -*-

"""Imbalanced dataset handling module
"""

from imblearn.under_sampling import RandomUnderSampler
from imblearn.under_sampling import NearMiss
from imblearn.under_sampling import TomekLinks
from imblearn.under_sampling import EditedNearestNeighbours

from imblearn.over_sampling import SMOTE

from imblearn.combine import SMOTEENN
from imblearn.combine import SMOTETomek

_resamplers = {
    'random_undersampling': RandomUnderSampler,
    'near_miss': NearMiss,
    'tomek': TomekLinks,
    'smote': SMOTE,
    'smoteenn': SMOTEENN,
    'smotetomek': SMOTETomek,
    'enn': EditedNearestNeighbours
}

def resample(resampler_type, X, y, params):
    resampler = _resamplers[resampler_type](**params) if params != None\
        else _resamplers[resampler_type]()
    return resampler.fit_resample(X, y)