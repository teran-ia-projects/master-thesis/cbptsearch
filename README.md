# Circumbinary Planets Machine Learning Transits Search Package


A package for circumbinary planet (CBP) transit detection based on Time Series Classificacion (TSC). ([Documentation](doc/TFM_TeranMirandaMarcelo.pdf))

<br />

<img src="doc/pipeline-overview.jpg" width="558.5" height="258">

<br />

## Matched filter based candidates extraction

Extacts a ML dataset from raw Kepler and TESS mission light curves.

Scripts [extraction.py](cbptsearch/extraction.py) and [sim_extraction.py](cbptsearch/sim_extraction.py) are used for transit-like candidate extraction.

<br />

<img src="doc/pipeline-sp-detail.jpg" width="558.5">

<br />

## TSC model training

Trains a TSC model in order to build a circumbinary transit classifier. (Using matched filter output dataset)

Script [training_pipeline.py](cbptsearch/training_pipeline.py) is used for model training. (Configuration file example: [run-example.yml](runs/run-example.yml) )

<br />

<img src="doc/pipeline-ml-training-detail.jpg" width="558.5">

<br />

Run example:

```shell
python3 ./cbptsearch/training_pipeline.py -d "/path/to/root/dir" -f run-example.yml -s True -n 1
```
Where:

- **d:** root directory (where uns, dataset and result folder were created)
- **f:** run configuration filename
- **s:** save flag (True or False)
- **n:** njobs config (parallel executions)

## TSC inference

Search for circubminary planet transits in eclipsing binary (EB) sistems light curves.

Implemented in [search.py](cbptsearch/search.py).

<br />

<img src="doc/pipeline-ml-inference-detail.jpg" width="558.5">

<br />

Run example:

```shell
python3 ./cbptsearch/search.py --t-depth=1 --thld=0.01 --mission=TESS --system="TOI 1338" \
                               --t-width="10, 14, 20, 24, 30, 34" --model=models/model-example.pkl \
                               --length=70 --type=4C --out-dir=results/inference
```
Where:

- **t-depth:** matched filter template depth
- **thld:** matched filter detection threshold
- **mission:** mission name (Kepler or TESS)
- **system:** input lightcurve system name
- **t-width:** matched filter template widths (comma separated string)
- **model:** ML model file path
- **length:** TS samples length normaliztion value
- **type:** ML model labeling type (2C, 2C_NO_BIN, 3C, 3C_CBP_EB2, 4C)
- **out-dir:** canidate results output directory
